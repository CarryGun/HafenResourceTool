#if 0
#include "modelpreview.h"
#include <QQuaternion>
#include <QPropertyAnimation>
#include <QWheelEvent>
#include <math.h>
#include "../tools/transformer.h"

ModelPreview::ModelPreview(const QUrl &meshUrl, const QUrl &texUrl)
{
    m_xOffset = 50.0f;
    // Root entity
    m_rootEntity = new Qt3DCore::QEntity;
    // Entity for model
    Qt3DCore::QEntity *myEntity = new Qt3DCore::QEntity(m_rootEntity);
    // Material for entity
    if (texUrl.isEmpty()) {
        Qt3DRender::QMaterial *material = new Qt3DExtras::QPhongMaterial(myEntity);
        myEntity->addComponent(material);
    } else {
        // Texture
        Qt3DExtras::QNormalDiffuseSpecularMapMaterial *material = new Qt3DExtras::QNormalDiffuseSpecularMapMaterial(myEntity);
        Qt3DRender::QTextureImage *texture = new Qt3DRender::QTextureImage();
        material->diffuse()->addTextureImage(texture);
        texture->setSource(texUrl);
        myEntity->addComponent(material);
    }
    // Model mesh
    Qt3DRender::QMesh *mesh = new Qt3DRender::QMesh(myEntity);
    // Load Vawefront file
    mesh->setSource(meshUrl);
    // Add mesh to root
    myEntity->addComponent(mesh);
    // Light
    Qt3DRender::QDirectionalLight *light = new Qt3DRender::QDirectionalLight(m_rootEntity);
    light->setWorldDirection(QVector3D(60.0f, -40.f, -60.0f));
    light->setIntensity(1.0f);
    light->setColor(QColor(238, 220, 165));
    myEntity->addComponent(light);
    // Transformation
    Qt3DCore::QTransform *transform = new Qt3DCore::QTransform(myEntity);
    Transformer *transformController = new Transformer(this);
    transformController->setTarget(transform);
    transformController->setRadius(0.0f);
    myEntity->addComponent(transform);
    QPropertyAnimation *rotateAnimation = new QPropertyAnimation(transform);
    rotateAnimation->setTargetObject(transformController);
    rotateAnimation->setPropertyName("angle");
    rotateAnimation->setStartValue(QVariant::fromValue(0));
    rotateAnimation->setEndValue(QVariant::fromValue(360));
    rotateAnimation->setDuration(5000);
    rotateAnimation->setLoopCount(-1);
    // Camera
    Qt3DRender::QCamera *cam = camera();
    cam->lens()->setPerspectiveProjection(30.0f, 16.0f/9.0f, 0.1f, 1000.0f);
    cam->setPosition(QVector3D(m_xOffset, 0.0f, 0.0f));
    cam->setViewCenter(QVector3D(0, 8, 0));
    //cam->rotate(QQuaternion((float)cos(M_PI / 4), (float)cos(M_PI / 4), 0.0f, 0.0f));
    setRootEntity(m_rootEntity);
    // Display
    show();
    rotateAnimation->start();
}

ModelPreview::~ModelPreview()
{
    delete m_rootEntity;
}

bool ModelPreview::event(QEvent *e)
{
    if (e->type() == QEvent::Close) {
        deleteLater();
    } else if (e->type() == QEvent::Wheel) {
        QWheelEvent *we = dynamic_cast<QWheelEvent *>(e);
        if (we) {
            m_xOffset += we->angleDelta().y() > 0 ? 5 : -5;
            camera()->setPosition(QVector3D(m_xOffset, 0.0f, 0.0f));
        }
    }
    return Qt3DExtras::Qt3DWindow::event(e);
}
#endif
