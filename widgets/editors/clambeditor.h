#ifndef CLAMBEDITOR_H
#define CLAMBEDITOR_H

#include "../editor.h"

class ClambEditor : public Editor
{
public:
    ClambEditor(Layer *l, QWidget *parent = 0);
};

#endif // CLAMBEDITOR_H
