#ifndef SKELEDITOR_H
#define SKELEDITOR_H

#include "../editor.h"

class SkelEditor : public Editor
{
public:
    SkelEditor(Layer *l, QWidget *parent = 0);
};

#endif // SKELEDITOR_H
