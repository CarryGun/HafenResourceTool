#ifndef RLINKEDITOR_H
#define RLINKEDITOR_H

#include "../editor.h"

class RlinkEditor : public Editor
{
public:
    RlinkEditor(Layer *l, QWidget *parent = 0);
};

#endif // RLINKEDITOR_H
