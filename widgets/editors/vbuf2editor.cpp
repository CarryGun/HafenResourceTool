#include "vbuf2editor.h"
#include <QDir>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPixmap>
#include <QPushButton>

#include "../../resources/layers/meshlayer.h"
#include "../../resources/layers/texlayer.h"
#include "../../resources/layers/vbuf2layer.h"

#include "../fields/listfield.h"

#include "../../tools/objexporter.h"
#include "../../tools/objimporter.h"
#include "../../tools/threeexporter.h"
#include "../../tools/xmlimporter.h"
#include "../modelpreview.h"

Vbuf2Editor::Vbuf2Editor(Layer* l, QWidget* parent)
    : Editor(l, parent)
{
    // Cast layer
    Vbuf2Layer* nl = dynamic_cast<Vbuf2Layer*>(l);
    m_resName = nl->resName();
    m_resName.replace("/", "_");

    // Set up fields widgets and add them to layout
    // sublayers
    QList<QString> sublayers = nl->sublayers();
    QList<QString> display;
    foreach(const QString& s, sublayers) display << QString("%1 (dots: %2; dimension: %3; vertexes/points: %4)")
                                                        .arg(s)
                                                        .arg(nl->sublayer(s).size())
                                                        .arg(nl->dimension(s))
                                                        .arg(nl->sublayer(s).size() / nl->dimension(s));
    ListField* f_sublayers = new ListField("sublayers", display, this);
    f_sublayers->setReadOnly(true);
    m_layout->addWidget(f_sublayers);

    // meshes
    QList<MeshLayer*> meshes = nl->meshes();
    f_meshes = new MeshChecklistField("meshes", meshes, this);
    m_layout->addWidget(f_meshes);
    // connect(f_meshes, SIGNAL(checkChanged()), this, SLOT(onMeshesCheckChanges()));

    // Temp layout
    QHBoxLayout* buttonLayout = new QHBoxLayout(this);
    // Export Vawefront
    QPushButton* vawefrontButton = new QPushButton("Export to obj", this);
    vawefrontButton->setToolTip("Export to *.obj file");
    connect(vawefrontButton, SIGNAL(clicked(bool)), SLOT(vawefrontClicked()));
    buttonLayout->addWidget(vawefrontButton);
    // Export to Three.js
    QPushButton* threeButton = new QPushButton("Export to three.js", this);
    vawefrontButton->setToolTip("Export to three.js supported format");
    connect(threeButton, SIGNAL(clicked(bool)), SLOT(threeClicked()));
    buttonLayout->addWidget(threeButton);
    // Import
    QPushButton* importXml = new QPushButton("Import from XML", this);
    importXml->setToolTip("Import model data from XML file");
    connect(importXml, SIGNAL(clicked(bool)), SLOT(importFromXmlClicked()));
    buttonLayout->addWidget(importXml);
    // Import
    QPushButton* importObj = new QPushButton("Import from OBJ", this);
    importXml->setToolTip("Import model data from OBJ file");
    connect(importObj, SIGNAL(clicked(bool)), SLOT(importFromObjClicked()));
    buttonLayout->addWidget(importObj);

    // Layout tricks
    buttonLayout->insertStretch(-1, 1);
    m_layout->addLayout(buttonLayout);
}

void Vbuf2Editor::vawefrontClicked()
{
    bool b = exportObj();
    if (b)
        QMessageBox::information(
            this, "Success", QString("Model has been exported to file '%1'").arg("/obj/" + m_resName + ".obj"));
    else
        QMessageBox::warning(this, "Error", "Failed to save file");
}

void Vbuf2Editor::threeClicked()
{
    bool b = exportThree();
    if (b)
        QMessageBox::information(
            this, "Success", QString("Model has been exported to file '%1'").arg("/obj/" + m_resName + ".json"));
    else
        QMessageBox::warning(this, "Error", "Failed to save file");
}

void Vbuf2Editor::importFromXmlClicked()
{
    Vbuf2Layer* nl = dynamic_cast<Vbuf2Layer*>(m_layer);
    QList<QString> sl = nl->sublayers();
    if (sl.length() != 3 || !sl.contains("pos") || !sl.contains("nrm") || !sl.contains("tex"))
    {
        QMessageBox::warning(
            this, "Warning", "Cannot import data from XML\nOnly 3 sublayers supported for now (nrm, pos, tex)");
        return;
    }
    if (nl->meshes().length() > 1)
    {
        QMessageBox::warning(this, "Warning", "Cannot import data from XML\nOnly models with 1 mesh supported for now");
        return;
    }
    QString f = QFileDialog::getOpenFileName(this, "Import model", QDir::currentPath(), "Xml files (*.xml)");
    if (f.isEmpty())
        return;

    XmlImporter imp;
    if (!imp.import(f))
    {
        qDebug() << imp.getError();
        return;
    }

    nl->setSublayerList("pos", imp.pos());
    nl->setSublayerList("nrm", imp.nrm());
    nl->setSublayerList("tex", imp.tex());

    MeshLayer* ml = nl->meshes().at(0);
    ml->setInd(imp.faces());
    onFieldChanged();
}

void Vbuf2Editor::importFromObjClicked()
{
    Vbuf2Layer* nl = dynamic_cast<Vbuf2Layer*>(m_layer);
    QList<QString> sl = nl->sublayers();
    if (sl.length() != 3 || !sl.contains("pos") || !sl.contains("nrm") || !sl.contains("tex"))
    {
        QMessageBox::warning(
            this, "Warning", "Cannot import data from OBJ\nOnly 3 sublayers supported for now (nrm, pos, tex)");
        return;
    }
    if (nl->meshes().length() > 1)
    {
        QMessageBox::warning(this, "Warning", "Cannot import data from OBJ\nOnly models with 1 mesh supported for now");
        return;
    }
    QString f = QFileDialog::getOpenFileName(this, "Import model", QDir::currentPath(), "Obj files (*.obj)");
    if (f.isEmpty())
        return;

    ObjImporter imp;
    if (!imp.import(f))
    {
        qDebug() << imp.getError() << imp.pos().size() << imp.nrm().size() << imp.tex().size();
        return;
    }

    nl->setSublayerList("pos", imp.pos());
    nl->setSublayerList("nrm", imp.nrm());
    nl->setSublayerList("tex", imp.tex());

    MeshLayer* ml = nl->meshes().at(0);
    ml->setInd(imp.faces());
    onFieldChanged();
}

bool Vbuf2Editor::exportObj()
{
    QList<MeshLayer*> m = f_meshes->checkedMeshes();
    if (!m.length())
        return false;

    ObjExporter exporter(dynamic_cast<Vbuf2Layer*>(m_layer));
    return exporter.saveAs("./obj/" + m_resName + ".obj", m);
}

bool Vbuf2Editor::exportThree()
{
    QList<MeshLayer*> m = f_meshes->checkedMeshes();
    if (!m.length())
        return false;

    ThreeExporter exporter(dynamic_cast<Vbuf2Layer*>(m_layer));
    return exporter.saveAs(m_resName.split('_').last(), "./obj/" + m_resName + ".json", m);
}
