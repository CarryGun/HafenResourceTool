#include "audio2editor.h"
#include <QHBoxLayout>
#include <QPushButton>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>

#include "../../resources/layers/audio2layer.h"

#include "../fields/floatfield.h"
#include "../fields/intfield.h"
#include "../fields/stringfield.h"

Audio2Editor::Audio2Editor(Layer *l, QWidget *parent)
    : Editor(l, parent)
{
    // Cast layer
    Audio2Layer *nl = dynamic_cast<Audio2Layer *>(l);

    // Set up fields widgets and add them to layout
    // ver (unsigned char)
    IntField *f_ver = new IntField("ver", nl->ver(), this, 0, 255);
    f_ver->setReadOnly(true);
    m_layout->addWidget(f_ver);
    // id (string)
    StringField *f_id = new StringField("id", nl->id(), this);
    m_layout->addWidget(f_id);
    // bvol (float)
    FloatField *f_bvol = new FloatField("bvol", nl->bvol(), this, 0, 1);
    f_bvol->setSingleStep(0.05);
    m_layout->addWidget(f_bvol);
    if (nl->ver() != 2)
        f_bvol->setReadOnly(true);

    // Temp layout
    QHBoxLayout *buttonLayout = new QHBoxLayout(this);
    // Save sound
    QPushButton *saveSound = new QPushButton("Save sound", this);
    saveSound->setToolTip("Save current sound data to disc");
    connect(saveSound, SIGNAL(clicked(bool)), SLOT(saveClicked()));
    buttonLayout->addWidget(saveSound);
    // Save sound
    QPushButton *loadSound = new QPushButton("Load sound", this);
    loadSound->setToolTip("Load sound data from disc");
    connect(loadSound, SIGNAL(clicked(bool)), SLOT(loadClicked()));
    buttonLayout->addWidget(loadSound);

    // Layout tricks
    buttonLayout->insertStretch(-1, 1);
    m_layout->addLayout(buttonLayout);

    // Add connections for fields
    connect(f_id, &StringField::fieldChanged, [nl, f_id, this](){nl->setId(f_id->getValue()); this->onFieldChanged();});
    connect(f_bvol, &FloatField::fieldChanged, [nl, f_bvol, this](){nl->setBvol(f_bvol->getValue()); this->onFieldChanged();});
}

void Audio2Editor::saveClicked()
{
    qDebug() << "ASDASD";
    QString sf = QFileDialog::getSaveFileName(this, "Save sound", "./saved", "Vorbis OGG (*.ogg)");
    if (sf.isEmpty())
        return;

    QFile of(sf);
    if (!of.open(QIODevice::WriteOnly)) {
        QMessageBox::critical(this, "Error", "Failed to save file");
        return;
    }
    Audio2Layer *nl = dynamic_cast<Audio2Layer *>(m_layer);
    of.write(nl->audio());
    of.close();
    QMessageBox::information(this, "Saved", "Sound successfully saved");
}

void Audio2Editor::loadClicked()
{
    QString sf = QFileDialog::getOpenFileName(this, "Load sound", QDir::currentPath(), "Vorbis OGG (*.ogg)");
    if (sf.isEmpty())
        return;

    QFile of(sf);
    if (!of.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(this, "Error", "Failed to load file");
        return;
    }
    Audio2Layer *nl = dynamic_cast<Audio2Layer *>(m_layer);
    nl->setAudio(of.readAll());
    of.close();
    onFieldChanged();
}
