#ifndef FOODEVEDITOR_H
#define FOODEVEDITOR_H

#include "../editor.h"

class FoodevEditor : public Editor
{
public:
    FoodevEditor(Layer *l, QWidget *parent = 0);
};

#endif // FOODEVEDITOR_H
