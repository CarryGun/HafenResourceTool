#ifndef VBUF2EDITOR_H
#define VBUF2EDITOR_H

#include "../editor.h"
#include "../fields/meshchecklistfield.h"
#include <QTextStream>

class Vbuf2Editor : public Editor
{
    Q_OBJECT

public:
    Vbuf2Editor(Layer* l, QWidget* parent = nullptr);

private slots:
    void vawefrontClicked();
    void threeClicked();
    void importFromXmlClicked();
    void importFromObjClicked();

private:
    MeshChecklistField* f_meshes;
    QString m_resName;

    bool exportObj();
    bool exportThree();
};

#endif // VBUF2EDITOR_H
