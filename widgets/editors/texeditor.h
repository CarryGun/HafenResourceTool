#ifndef TEXEDITOR_H
#define TEXEDITOR_H

#include "../editor.h"

class TexEditor : public Editor
{
public:
    TexEditor(Layer *l, QWidget *parent = 0);
};

#endif // TEXEDITOR_H
