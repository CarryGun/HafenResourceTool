#ifndef MAT2EDITOR_H
#define MAT2EDITOR_H

#include "../editor.h"

class Mat2Editor : public Editor
{
public:
    Mat2Editor(Layer *l, QWidget *parent = 0);
};

#endif // MAT2EDITOR_H
