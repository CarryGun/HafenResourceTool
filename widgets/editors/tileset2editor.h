#ifndef TILESET2EDITOR_H
#define TILESET2EDITOR_H

#include "../editor.h"

class Tileset2Editor : public Editor
{
public:
    Tileset2Editor(Layer *l, QWidget *parent = 0);
};

#endif // TILESET2EDITOR_H
